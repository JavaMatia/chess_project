#include "Base_Player.h"
#define SIZE 8

/*
the constructor
*/
Base_Player::Base_Player(const int x, const int y, const char type)
{
	this->x = x;
	this->y = y;
	this->type = type;
}


/*
The function checks if the move is valid - if the new x,y is greater than 8 or less than 0, the function returns false (TODO: when can this happen? (5 error code)
Input: the new x and y (int)
Output: true for valid move, false otherwise
*/
bool Base_Player::inBounds(const int x, const int y) const
{
	if (x > BOUND || x < 0 || y > BOUND || y + y < 0)
	{
		return false;
	}
	return true;
}

//setters

void Base_Player::setX(const int x)
{
	this->x = x;
}

void Base_Player::setY(const int y)
{
	this->y = y;
}
//--------------------------------------------------//
void Base_Player::setType(const char type)
{
	this->type = type;
}

//getters:

int Base_Player::getX() const
{
	return this->x;
}

int Base_Player::getY() const
{
	return this->y;
}

char Base_Player::getType() const
{
	return this->type;
}

/*
gets the destination x and y and checks if it's the same insex as the current insex
input: the new x and y
output:returns true if it's the same, false otherwise
*/
bool Base_Player::SameIndex(const int x, const int y)
{
	if (x == this->x && y == this->y)
	{
		return true;
	}
	return false;
}

/*
the function checks if the destination target is the same color as the current player
input: the type of the destination target
output: true if they are the same, false otherwise
*/
bool Base_Player::SameColor(const char type)
{
	return (this->type >= 'a' && type >= 'a' || this->type <= 'Z' && type <= 'Z');
}

/*
The function checks if the player chosen is the active player
*/
bool Base_Player::currentPlayer(const char player)
{
	return ((player == BLACK && this->type >= 'a' && this->type <= 'z') || (player == WHITE && this->type >= 'A' && this->type <= 'Z'));
}

/*
The function performs a basic movement check, and returns error code or success code accordingly
Input: destination x, y and the board. kingCheck tells us if it is the king checking if this piece is a threat, it does not actually move the piece
Output: error codes, '0' if succeesful
*/
char Base_Player::BasicCheck(const int x, const int y, Board& board, bool kingCheck)
{

	if (!(this->inBounds(x, y)))
	{
		return OUT_OF_BOUNDS;
	}
	if (this->SameIndex(x, y))
	{
		return SAME_INDEX;
	}
	if (!(this->currentPlayer(board.getCurrentPlayer())) && !kingCheck) // if we passed kingCheck, so it is ok, skip
	{
		return UNMATCHED_TURN;
	}
	if (board.getBoard()[x*SIZE + y] == NULL)
	{
		return OK;
	}
	if (this->SameColor(board.getBoard()[x*SIZE + y]->getType()))
	{
		return INVALID_TARGET;
	}
	return OK;
}

/*
The function checks for threats for the king. It tells us if we can move a piece without leaving our king compromised
*/
char Base_Player::checkThreats(Board & board)
{
	// if we are checking for white for example, so the 'enemy' is now the black.
	board.switchPlayer();
	for (int i = 0; i < SIZE; i++)
	{
		for (int j = 0; j < SIZE; j++)
		{
			if (board.getBoard()[i*SIZE + j] != NULL)
			{
				if (board.getCurrentPlayer() == WHITE) // if we are CHECKING fpr the white players
				{
					if (board.getBoard()[i*SIZE + j]->getType() >= 'A' && board.getBoard()[i*SIZE + j]->getType() <= 'Z') // and this piece is an enemy (black)
					{
						if (OK == board.getBoard()[i*SIZE + j]->move(board.getBlackX(), board.getBlackY(), board, true)) // check if this piece can kill the king
						{
							board.switchPlayer();
							return KING_IS_UNDER_THREAT;
						}
					}
				}
				else
				{
					if (board.getBoard()[i*SIZE + j]->getType() >= 'a' && board.getBoard()[i*SIZE + j]->getType() <= 'z')
					{
						if (OK == board.getBoard()[i*SIZE + j]->move(board.getWhiteX(), board.getWhiteY(), board, true))
						{
							board.switchPlayer();
							return KING_IS_UNDER_THREAT;
						}
					}
				}
			}
		}
	}
	board.switchPlayer();
	return OK;
}

/*
The function checks if the move we made is a check to the other king
*/
char Base_Player::enemyKingInDanger(Board & board)
{
	for (int i = 0; i < SIZE; i++)
	{
		for (int j = 0; j < SIZE; j++)
		{
			if (board.getBoard()[i*SIZE + j] != NULL)
			{
				if (board.getCurrentPlayer() == WHITE) // if the current turn is for the white
				{
					if (board.getBoard()[i*SIZE + j]->getType() >= 'A' && board.getBoard()[i*SIZE + j]->getType() <= 'Z') // this is piece is white
					{
						if (OK == board.getBoard()[i*SIZE + j]->move(board.getBlackX(), board.getBlackY(), board, true)) //try to eat 
						{
							return ENEMY_CHECK;
						}
					}
				}
				else
				{
					if (board.getBoard()[i*SIZE + j]->getType() >= 'a' && board.getBoard()[i*SIZE + j]->getType() <= 'z')
					{
						if (OK == board.getBoard()[i*SIZE + j]->move(board.getWhiteX(), board.getWhiteY(), board, true))
						{
							return ENEMY_CHECK;
						}
					}
				}
			}
		}
	}
	return OK;
}


