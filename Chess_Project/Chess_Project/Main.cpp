#include <iostream>
#include <string>
#include <windows.h>
#include <time.h>
#include "Board.h"
#include "Pipe.h"
#define QUIT "quit"
#define UNMATCHED_PLAYER '2'
#define CHESS_WILL_OCCUR '1'
#define CHECK_MATE '8'
#define SIZE 8

std::string parseCoordinates(std::string msgFromFrontend);
int main()
{
	Pipe pipe;
	bool isConnected;
	char msgToGraphics[1024];
	Board* board;
	char ans[2];
	std::string msgFromGraphics;
	std::string coordinates = "0000";
	strcpy_s(msgToGraphics, "rnbkqbnrpppppppp################################PPPPPPPPRNBKQBNR0");

	ShellExecute(NULL, "open", "chessGraphics.exe", NULL, NULL, SW_SHOWDEFAULT);
	Sleep(1500);
	isConnected = pipe.connect();
	if (!isConnected)
	{
		std::cerr << "Couldn't find frontend graphics. Terminating now.";
		pipe.close();
		getchar();
		exit(1);
	}
	board = new Board(msgToGraphics);
	board->printBord();


	pipe.sendMessageToGraphics(msgToGraphics);
	msgFromGraphics = pipe.getMessageFromGraphics();
	while (msgFromGraphics != QUIT)
	{
		coordinates = parseCoordinates(msgFromGraphics);
		if (board->getBoard()[(coordinates[0] - '0') * SIZE + (coordinates[1] - '0')] != NULL)
		{
			ans[0] = board->getBoard()[(coordinates[0] - '0') * SIZE + (coordinates[1] - '0')]->move(coordinates[2] - '0', coordinates[3] - '0', *board, false);
		}
		else
		{
			ans[0] = UNMATCHED_PLAYER;	
		}
		ans[1] = NULL;

		if (ans[0] == OK || ans[0] == CHESS_WILL_OCCUR || ans[0] == CHECK_MATE) //if it was a valid move, switch turns
		{
			//changing the players turn
			board->switchPlayer();
		}
		board->printBord();

		// return result to graphics		
		pipe.sendMessageToGraphics(ans);

		// get message from graphics
		msgFromGraphics = pipe.getMessageFromGraphics();


	}
	pipe.close();
	delete board;
	return 0;
}

/*
the function translates the coordinates from the frontend to coordinats the backend can understand
Input: coordintaes we desire to convert
Output: the converted coordinates
*/
std::string parseCoordinates(std::string msgFromFrontend)
{
	std::string converted = "0000";
	for (int i = 0; i < 4; i++)
	{
		if (msgFromFrontend[i] == 'a' || msgFromFrontend[i] == '8')
		{
			converted[i] = '0';
		}
		else if (msgFromFrontend[i] == 'b' || msgFromFrontend[i] == '7')
		{
			converted[i] = '1';
		}
		else if (msgFromFrontend[i] == 'c' || msgFromFrontend[i] == '6')
		{
			converted[i] = '2';
		}
		else if (msgFromFrontend[i] == 'd' || msgFromFrontend[i] == '5')
		{
			converted[i] = '3';
		}
		else if (msgFromFrontend[i] == 'e' || msgFromFrontend[i] == '4')
		{
			converted[i] = '4';
		}
		else if (msgFromFrontend[i] == 'f' || msgFromFrontend[i] == '3')
		{
			converted[i] = '5';
		}
		else if (msgFromFrontend[i] == 'g' || msgFromFrontend[i] == '2')
		{
			converted[i] = '6';
		}
		else if (msgFromFrontend[i] == 'h' || msgFromFrontend[i] == '1')
		{
			converted[i] = '7';
		}
	}
	return converted;
}
