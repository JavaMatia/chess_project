#pragma once
#include "Base_Player.h"
#include <string>
#include <iostream>

#define BOARD_SIZE 8
#define MSG_LENGTH 64
//Pieces:
#define WHITE_ROOK 'R'
#define BLACK_ROOK 'r'
#define WHITE_KING 'K'
#define BLACK_KING 'k'
#define BLACK_PAWN 'p'
#define WHITE_PAWN 'P'
#define WHITE_KNIGHT 'N'
#define BLACK_KNIGHT 'n'
#define WHITE_BISHOP 'B'
#define BLACK_BISHOP 'b'
#define BLACK_QUEEN 'q'
#define WHITE_QUEEN 'Q'
//initial king positions
#define KING_START_X_BLACK 3
#define KING_START_X_WHITE 3
#define KING_START_Y_BLACK 0
#define KING_START_Y_WHITE 7

class Base_Player;
class Board
{
public:
	Board(std::string msg);

	void SetCurrentPlayer(char player);
	Base_Player** getBoard();
	char getCurrentPlayer() const;
	void printBord();
	void switchPlayer();
	//getters:
	int getBlackX() const;
	int getBlackY() const;
	int getWhiteX() const;
	int getWhiteY() const;
	//setters
	void setBlackX(const int blackX);
	void setBlackY(const int blackY);
	void setWhiteX(const int whiteX);
	void setWhiteY(const int whiteY);
	~Board();
private:
	//the positions of the king
	int blackX;
	int blackY;
	int whiteX;
	int whiteY;
	Base_Player* board[BOARD_SIZE][BOARD_SIZE];
	char currentPlayer; // 1 - black player 0 - white player
};

