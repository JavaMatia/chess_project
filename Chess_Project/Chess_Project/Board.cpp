#include "Board.h"
#include "Rook.h"
#include "King.h"
#include "Pawn.h"
#include "Knight.h"
#include "Bishop.h"
#include "Queen.h"

Board::Board(std::string msg)
{
	this->blackX = KING_START_X_BLACK;
	this->blackY = KING_START_Y_BLACK;
	this->whiteX = KING_START_X_WHITE;
	this->whiteY = KING_START_Y_WHITE;

	int k = 0;
	this->currentPlayer = msg[MSG_LENGTH];
	for (int i = 0; i < BOARD_SIZE; i++)
	{
		for (int j = 0; j < BOARD_SIZE; j++)
		{
			switch (msg[k])
			{
			case WHITE_ROOK:
				this->board[j][i] = new Rook(j, i, WHITE_ROOK);
				break;
			case BLACK_ROOK:
				this->board[j][i] = new Rook(j, i, BLACK_ROOK);
				break;
			case WHITE_KING:
				this->board[j][i] = new King(j, i, WHITE_KING);
				break;
			case BLACK_KING:
				this->board[j][i] = new King(j, i, BLACK_KING);
				break;
			case WHITE_PAWN:
				this->board[j][i] = new Pawn(j, i, WHITE_PAWN);
				break;
			case BLACK_PAWN:
				this->board[j][i] = new Pawn(j, i, BLACK_PAWN);
				break;
			case BLACK_KNIGHT:
				this->board[j][i] = new Knight(j, i, BLACK_KNIGHT);
				break;
			case WHITE_KNIGHT:
				this->board[j][i] = new Knight(j, i, WHITE_KNIGHT);
				break;
			case WHITE_BISHOP:
				this->board[j][i] = new Bishop(j, i, WHITE_BISHOP);
				break;
			case BLACK_BISHOP:
				this->board[j][i] = new Bishop(j, i, BLACK_BISHOP);
				break;
			case BLACK_QUEEN:
				this->board[j][i] = new Queen(j, i, BLACK_QUEEN);
				break;
			case WHITE_QUEEN:
				this->board[j][i] = new Queen(j, i, WHITE_QUEEN);
				break;
			default:
				this->board[j][i] = NULL;
				break;
			}
			if (k < MSG_LENGTH-1)
			{
				k++;
			}
		}
	}
}

//--------------------------------------------------//
//setter
void Board::SetCurrentPlayer(char player)
{
	this->currentPlayer = player;
}

Base_Player** Board::getBoard()
{
	return *this->board;
}

void Board::setBlackX(const int blackX)
{
	this->blackX = blackX;
}

void Board::setBlackY(const int blackY)
{
	this->blackY = blackY;
}

void Board::setWhiteX(const int whiteX)
{
	this->whiteX = whiteX;
}

void Board::setWhiteY(const int whiteY)
{
	this->whiteY = whiteY;
}
//--------------------------------------------------//

//getters:
char Board::getCurrentPlayer() const
{
	return this->currentPlayer;
}

int Board::getBlackX() const
{
	return this->blackX;
}

int Board::getBlackY() const
{
	return this->blackY;
}

int Board::getWhiteX() const
{
	return this->whiteX;
}

int Board::getWhiteY() const
{
	return this->whiteY;
}
//--------------------------------------------------//
/*
The function prints the board
*/
void Board::printBord()
{
	for (int i = 0; i < BOARD_SIZE; i++)
	{
		for (int j = 0; j < BOARD_SIZE; j++)
		{
			if (this->board[j][i] == NULL)
			{
				std::cout << "0 ";
			}
			else
			{
				std::cout << this->board[j][i]->getType();
				std::cout << " ";
			}
		}
		std::cout << "\n";
		
	}
}
//-------------------------------------------------//
/*
The function switches the active player 
*/
void Board::switchPlayer()
{
	if (this->currentPlayer == WHITE)
	{
		this->currentPlayer = BLACK;
	}
	else
	{
		this->currentPlayer = WHITE;
	}
}

//--------------------------------------------------//

/*
The destructor frees all allocated memory of the board
*/
Board::~Board()
{
	for (int i = 0; i < BOARD_SIZE; i++)
	{
		for (int j = 0; j < BOARD_SIZE; j++)
		{
			if (this->board[i][j] != NULL)
			{
				delete this->board[i][j];
			}
		}
		//delete[] this->board[i];
	}
	//delete[] this->board;
}