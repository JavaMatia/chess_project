#include "Bishop.h"



Bishop::Bishop(const int x, const int y, const char type) : Base_Player::Base_Player(x, y, type)
{
}

char Bishop::move(int x, int y, Board & board, bool kingCheck)
{

	int oldX = this->x, oldY = this->y;
	char oldType = this->type, ans;
	int distance = 0;
	bool xMove, movementX, movementY, yMove;


	ans = this->BasicCheck(x, y, board, kingCheck);

	if (ans == OK)
	{
		movementX = (this->x > x);
		movementY = (this->y > y);
		xMove = (this->x != x);
		yMove = (this->y != y);
		if (abs(this->x - x) != abs(this->y - y))
		{
			return INVALID_PIECE_MOVE;
		}
		else if (movementX)
		{
			distance = abs(this->x - x);
			for (int i = 1; i < distance; i++)
			{
				if (movementY)
				{
					if (board.getBoard()[(this->x - i) * SIZE + this->y - i] != NULL)
					{
						return INVALID_PIECE_MOVE;
					}
				}
				else
				{
					if (board.getBoard()[(this->x - i) * SIZE + this->y + i] != NULL)
					{
						return INVALID_PIECE_MOVE;
					}
				}
			}
		}
		else
		{
			distance = abs(this->x - x);
			for (int i = 1; i < distance; i++)
			{
				if (movementY)
				{
					if (board.getBoard()[(this->x + i) * SIZE + this->y - i] != NULL)
					{
						return INVALID_PIECE_MOVE;
					}
				}
				else
				{
					if (board.getBoard()[(this->x + i) * SIZE + this->y + i] != NULL)
					{
						return INVALID_PIECE_MOVE;
					}
				}
			}
		}

		if (!kingCheck) // if it is NOT a king check, we actually want to move the pieces
		{
			//check if the target square is not empty and delete it
			if (board.getBoard()[x * SIZE + y] != NULL)
			{
				delete board.getBoard()[x * SIZE + y];
			}
			board.getBoard()[x * SIZE + y] = new Bishop(x, y, this->type);
			delete board.getBoard()[this->x * SIZE + this->y];
			board.getBoard()[oldX * SIZE + oldY] = NULL;
			if (this->checkThreats(board) == KING_IS_UNDER_THREAT) // if the new position of the rook is a threat for our king, revert!
			{
				ans = KING_IS_UNDER_THREAT;
				board.getBoard()[oldX * SIZE + oldY] = new Bishop(oldX, oldY, oldType);
				delete board.getBoard()[x * SIZE + y];
				board.getBoard()[x * SIZE + y] = NULL;
			}
			else // if our king was under threat, no need to check if the enemy king is in danger
			{
				ans = this->enemyKingInDanger(board);
			}
		}
	}
	return ans;
}