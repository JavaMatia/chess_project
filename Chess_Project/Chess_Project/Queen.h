#pragma once
#include "Base_Player.h"
#define SIZE 8
#define INVALID_PIECE_MOVE '6'
#define OK '0'
class Queen : public Base_Player
{
public:
	Queen(const int x, const int y, const char type);
	char move(int x, int y, Board& board, bool kingCheck = false);
};

