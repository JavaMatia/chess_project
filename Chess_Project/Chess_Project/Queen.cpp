#include "Queen.h"
#include "Rook.h"
#include "bishop.h"


/*
The constructor
*/
Queen::Queen(const int x, const int y, const char type) : Base_Player::Base_Player(x, y, type)
{
}

char Queen::move(int x, int y, Board & board, bool kingCheck)
{
	int oldX = this->x, oldY = this->y;
	char oldType = this->type, ans;

	ans = this->BasicCheck(x, y, board, kingCheck);

	//Check if the queen moved like a rook:
	Rook* rook = new Rook(this->x, this->y, this->type);
	Bishop* bishop = new Bishop(this->x, this->y, this->type);
	if (rook->move(x, y, board, true) == INVALID_PIECE_MOVE && bishop->move(x, y, board, true) == INVALID_PIECE_MOVE)
	{
		delete rook;
		delete bishop;
		return INVALID_PIECE_MOVE;
	}
	delete rook;
	delete bishop;

	if (!kingCheck) // if it is NOT a king check, we actually want to move the pieces
	{
		//check if the target square is not empty and delete it
		if (board.getBoard()[x * SIZE + y] != NULL)
		{
			delete board.getBoard()[x * SIZE + y];
		}
		board.getBoard()[x * SIZE + y] = new Queen(x, y, this->type);
		delete board.getBoard()[this->x * SIZE + this->y];
		board.getBoard()[oldX * SIZE + oldY] = NULL;
		if (this->checkThreats(board) == KING_IS_UNDER_THREAT) // if the new position of the rook is a threat for our king, revert!
		{
			ans = KING_IS_UNDER_THREAT;
			board.getBoard()[oldX * SIZE + oldY] = new Queen(oldX, oldY, oldType);
			delete board.getBoard()[x * SIZE + y];
			board.getBoard()[x * SIZE + y] = NULL;
		}
		else // if our king was under threat, no need to check if the enemy king is in danger
		{
			ans = this->enemyKingInDanger(board);
		}
	}

	return ans;
}
