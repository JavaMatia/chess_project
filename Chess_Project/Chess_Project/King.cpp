#include "King.h"
#include <cmath>



King::King(const int x, const int y, const char type) : Base_Player::Base_Player(x, y, type)
{
}

/*
The move function for the king
*/
char King::move(int x, int y, Board & board, bool kingCheck)
{
	int distanceX = 0;
	int distanceY = 0;
	int oldX = this->x, oldY = this->y;
	char oldType = this->type;
	char ans = this->BasicCheck(x, y, board, kingCheck); //basic check for invalid movement
	if (ans == OK)
	{
		distanceX = abs(this->x - x);
		distanceY = abs(this->y - y);
		if (distanceX > 1 || distanceY > 1) // did the king moved more then on cube
		{
			return INVALID_PIECE_MOVE;
		}
		if (!kingCheck) // if it wasn't a checked call, try to move the king
		{
			if (board.getBoard()[x * SIZE + y] != NULL)
			{
				delete board.getBoard()[x * SIZE + y];
			}
			board.getBoard()[x * SIZE + y] = new King(x, y, this->type);
			delete board.getBoard()[this->x * SIZE + this->y];
			board.getBoard()[oldX * SIZE + oldY] = NULL;

			//update the global variables of the king location in board, then resore them if we need to
			if (oldType == BLACK_KING)
			{
				board.setBlackX(x);
				board.setBlackY(y);
			}
			else if (oldType == WHITE_KING)
			{
				board.setWhiteX(x);
				board.setWhiteY(y);
			}
			if (this->checkThreats(board) == KING_IS_UNDER_THREAT) // after that move, the king is under threat?
			{
				ans = KING_IS_UNDER_THREAT;
				board.getBoard()[oldX * SIZE + oldY] = new King(oldX, oldY, oldType);
				delete board.getBoard()[x * SIZE + y];
				board.getBoard()[x * SIZE + y] = NULL;
				if (oldType == BLACK_KING)
				{
					board.setBlackX(oldX);
					board.setBlackY(oldY);
				}
				else if (oldType == WHITE_KING)
				{
					board.setWhiteX(oldX);
					board.setWhiteY(oldY);
				}
			}
			else
			{
				ans = this->enemyKingInDanger(board); //small chance our king will put the enemy king in danger, but just in case
			}

		}
	}
	return ans;
}

///*
//The function checks for chess-mate;
//*/
//bool King::checkMate(Board & board)
//{
//	int ans = 0, distanceX = 0, distanceY = 0;
//	for (int i = 0; i < SIZE; i++)
//	{
//		for (int j = 0; j < SIZE; j++)
//		{
//			ans = this->BasicCheck(i, j, board, false);
//			if (ans == OK)
//			{
//				distanceX = abs(this->x - x);
//				distanceY = abs(this->y - y);
//				if (distanceX == 1 || distanceY == 1) // can the king move?
//				{
//					if (this->checkThreats(board) == OK)
//					{
//						return false; // the king can move, it is not mate
//					}
//				}
//			}
//		}
//	}
//	return true;
//}

