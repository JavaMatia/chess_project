#pragma once
#define BOUND 8
//error codes
#define SAME_INDEX '7'
#define OUT_OF_BOUNDS '5'
#define KING_IS_UNDER_THREAT '4'
#define INVALID_TARGET '3'
#define UNMATCHED_TURN '2'
#define ENEMY_CHECK '1'
#define OK '0'
#define BLACK '1'
#define WHITE '0'

#include "Board.h"

class Board;
class Base_Player
{
public:
	Base_Player(const int x, const int y, const char type);
	bool inBounds(const int x, const int y) const;
	bool SameIndex(const int x, const int y);
	bool SameColor(const char type);
	bool currentPlayer(const char player);
	char BasicCheck(const int x, const int y, Board& board, bool kingCheck = false);
	char checkThreats(Board& board);
	char enemyKingInDanger(Board& board);

	//setters
	void setX(const int x);
	void setY(const int y);
	void setType(const char type);
	//getters
	int getX() const;
	int getY() const;
	char getType() const;
	//virtuals:
	virtual char move(int x, int y, Board& board, bool kingCheck=false) = 0;
protected:
	int x;
	int y;
	char type; // if it's a capital letter - it is a white player, otherwise it is black
};

