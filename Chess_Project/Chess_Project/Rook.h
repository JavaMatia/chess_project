#pragma once
#include "Base_Player.h"
#include "Board.h"
#define SYMBOL 'r'
#define INVALID_PIECE_MOVE '6'
#define OK '0'
#define SIZE 8


class Board;
class Rook : public Base_Player
{
public:
	Rook(const int x, const int y, const char type);
	char move(int x, int y, Board& board, bool kingCheck);
};

