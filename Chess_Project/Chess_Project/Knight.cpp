#include "Knight.h"


/*
The constructor
*/
Knight::Knight(const int x, const int y, const char type) : Base_Player::Base_Player(x, y, type)
{
}

/*
The function is incharge of moving a rook. It uses the basic check function from Base_Player and implements extra check
*/
char Knight::move(int x, int y, Board& board, bool kingCheck)
{
	int distance = 0;
	int oldX = this->x, oldY = this->y;
	char oldType = this->type;

	char ans = this->BasicCheck(x, y, board, kingCheck); // basic movement check kingCheck is false, we want to actually move the pieces

	//Deep check
	if (ans == OK)
	{
		//if the knight did go two on the x and one on the y OR two on the y and on the x, it is an invalid move!
		if ((abs(this->x -x) == 2 && abs(this->y - y) == 1) || (abs(this->y - y) == 2 && abs(this->x - x) == 1))
		{
			//this piece of code is about the same in all the pieces, but has small changes in some of them
			if (!kingCheck) // if it is NOT a king check, we actually want to move the pieces
			{
				//check if the target square is not empty and delete it
				if (board.getBoard()[x * SIZE + y] != NULL)
				{
					delete board.getBoard()[x * SIZE + y];
				}
				board.getBoard()[x * SIZE + y] = new Knight(x, y, this->type);
				delete board.getBoard()[this->x * SIZE + this->y];
				board.getBoard()[oldX * SIZE + oldY] = NULL;
				if (this->checkThreats(board) == KING_IS_UNDER_THREAT) // if the new position of the rook is a threat for our king, revert!
				{
					ans = KING_IS_UNDER_THREAT;
					board.getBoard()[oldX * SIZE + oldY] = new Knight(oldX, oldY, oldType);
					delete board.getBoard()[x * SIZE + y];
					board.getBoard()[x * SIZE + y] = NULL;
				}
				else // if our king was under threat, no need to check if the enemy king is in danger
				{
					ans = this->enemyKingInDanger(board);
				}
			}
		}
		else
		{
			return INVALID_PIECE_MOVE;
		}
		
	}
	return ans;

}


