#pragma once
#include "Base_Player.h"
#define SIZE 8
#define INVALID_PIECE_MOVE '6'
class Knight : public Base_Player
{
public:
	Knight(const int x, const int y, const char type);
	//virtual:
	char move(int x, int y, Board& board, bool kingCheck = false);
};

