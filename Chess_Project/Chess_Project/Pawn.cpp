#include "Pawn.h"


/*
The constructor
*/
Pawn::Pawn(const int x, const int y, const char type, bool hasMoved) : Base_Player::Base_Player(x, y, type)
{
	this->hasMoved = hasMoved; // the rook hasn't moved if the game just initialized
}

//getter
bool Pawn::getHasMoved() const
{
	return this->hasMoved;
}

//setter
void Pawn::setHasMoved(const bool hasMoved)
{
	this->hasMoved = hasMoved;
}

/*
The function is resposible for the movement of the pawn
Input: destination x, y, board and if it is the king checking if something can it him
Output: one of the error codes, 0 if the movement was valid
*/
char Pawn::move(int x, int y, Board & board, bool kingCheck)
{
	int distance = 0;
	bool xMove;
	bool hasEaten = false;
	int oldX = this->x, oldY = this->y;
	char oldType = this->type;

	char ans = this->BasicCheck(x, y, board, kingCheck); // basic movement check kingCheck is false, we want to actually move the pieces
	//Deep check
	if (ans == OK)
	{

		xMove = (this->x != x);

		//we can't go more than two squares, in any situation
		if (abs(this->y - y) > 2)
		{
			return INVALID_PIECE_MOVE;
		}
		//don't go backwards!
		if (board.getCurrentPlayer() == WHITE && y - this->y > 0 || board.getCurrentPlayer() == BLACK && y - this->y < 0)
		{
			return INVALID_PIECE_MOVE;
		}

		// if the pawn has moved on x and y and the target square is empty, return 6
		if ((abs(this->x - x) == 1) && (abs(this->y - y) == 1) && board.getBoard()[x*SIZE + y] == NULL) 
		{
			return INVALID_PIECE_MOVE;

		}

		// if the pawn has moved on x and y and the target square is not empty, skip the other checks
		else if ((abs(this->x - x) == 1) && (abs(this->y - y) == 1) && board.getBoard()[x*SIZE + y] != NULL) 
		{
			hasEaten = true;
		}
		// we can't move on x axis
		if (xMove && !hasEaten)
		{
			return INVALID_PIECE_MOVE;
		}

		//check if the square in fron of my is taken by enemy piece
		if (board.getBoard()[x*SIZE+y] != NULL && !hasEaten) 
		{
			return INVALID_PIECE_MOVE;
		}

		// the pawn moved two squares, but he already moved. OR - THERE IS A piece one y before the target square if we can jump twice, so it is not ok
		if (abs(this->y - y)  == 2 && this->hasMoved || 
			(abs(this->y - y) == 2 && board.getCurrentPlayer() == BLACK && board.getBoard()[x*SIZE + (y-1)] !=NULL) 
			|| (abs(this->y - y) == 2 && board.getCurrentPlayer() == WHITE && board.getBoard()[x*SIZE + (y + 1)] != NULL))
		{ 
			return INVALID_PIECE_MOVE;
		}


		if (!kingCheck) // if it is NOT a king check, we actually want to move the pieces
		{
			this->hasMoved = true;
			if (board.getBoard()[x * SIZE + y] != NULL)
			{
				delete board.getBoard()[x * SIZE + y];
			}
			board.getBoard()[x * SIZE + y] = new Pawn(x, y, this->type, true);
			delete board.getBoard()[this->x * SIZE + this->y];
			board.getBoard()[oldX * SIZE + oldY] = NULL;
			if (this->checkThreats(board) == KING_IS_UNDER_THREAT) // if the new position of the pawn is a threat for our king, revert!
			{
				ans = KING_IS_UNDER_THREAT;
				board.getBoard()[oldX * SIZE + oldY] = new Pawn(oldX, oldY, oldType);
				delete board.getBoard()[x * SIZE + y];
				board.getBoard()[x * SIZE + y] = NULL;
			}
			else // if our king was under threat, no need to check if the enemy king is in danger
			{
				ans = this->enemyKingInDanger(board);
			}
		}
	}
	return ans;
}

