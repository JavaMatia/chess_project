#pragma once
#include "Base_Player.h"

#define INVALID_PIECE_MOVE '6'
#define OK '0'
#define SIZE 8
class Pawn :
	public Base_Player
{
public:
	Pawn(const int x, const int y, const char type, bool hasMoved = false);
	bool getHasMoved() const;
	void setHasMoved(const bool hasMoved);
	//virtual
	char move(int x, int y, Board& board, bool kingCheck);
private:
	bool hasMoved;
};

