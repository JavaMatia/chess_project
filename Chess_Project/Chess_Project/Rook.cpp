#include "Rook.h"
#include <cmath> 

/*
The rook constructor - in simply uses the Base_Player constructor
*/
Rook::Rook(const int x, const int y, const char type) : Base_Player::Base_Player(x, y, type)
{
}

/*
The function is incharge of moving a rook. It uses the basic check function from Base_Player and implements extra check
*/
char Rook::move(int x, int y, Board& board, bool kingCheck = false)
{
	int i;
	bool movement;
	int distance = 0;
	bool xMove;
	bool yMove;
	int oldX = this->x, oldY = this->y;
	char oldType = this->type;

	char ans = this->BasicCheck(x, y, board,kingCheck); // basic movement check kingCheck is false, we want to actually move the pieces

	//Deep check
	if (ans == OK)
	{
		// x and y moved
		xMove = (this->x != x);
		yMove = (this->y != y);
		if (xMove && yMove) // The rook has moved on the x and y axis, not good
		{
			return INVALID_PIECE_MOVE;
		}
		else if (xMove) // movement on x axis
		{
			movement = (this->x > x);
			distance = abs(this->x - x);
			for (i = 1; i < distance; i++)
			{
				if (movement)
				{
					if (board.getBoard()[(this->x - i) * SIZE + this->y] != NULL)
					{
						return INVALID_PIECE_MOVE;
					}
				}
				else
				{
					if (board.getBoard()[(this->x + i) * SIZE + (this->y)] != NULL)
					{
						return INVALID_PIECE_MOVE;
					}
				}
			}
		}
		else
		{
			movement = (this->y > y);
			distance = abs(this->y - y);
			for (i = 1; i < distance; i++)
			{
				if (movement)
				{
					if (board.getBoard()[(this->x * SIZE) + (this->y - i)] != NULL)
					{
						return INVALID_PIECE_MOVE;
					}
				}
				else
				{
					if (board.getBoard()[this->x * SIZE + (this->y + i)] != NULL)
					{
						return INVALID_PIECE_MOVE;
					}
				}
			}
		}

		//this piece of code is about the same in all the pieces, but has small changes in some of them
		if (!kingCheck) // if it is NOT a king check, we actually want to move the pieces
		{
			//check if the target square is not empty and delete it
			if (board.getBoard()[x * SIZE + y] != NULL)
			{
				delete board.getBoard()[x * SIZE + y];
			}
			board.getBoard()[x * SIZE + y] = new Rook(x, y, this->type);
			delete board.getBoard()[this->x * SIZE + this->y];
			board.getBoard()[oldX * SIZE + oldY] = NULL;
			if (this->checkThreats(board) == KING_IS_UNDER_THREAT) // if the new position of the rook is a threat for our king, revert!
			{
				ans = KING_IS_UNDER_THREAT;
				board.getBoard()[oldX * SIZE + oldY] = new Rook(oldX, oldY, oldType);
				delete board.getBoard()[x * SIZE + y];
				board.getBoard()[x * SIZE + y] = NULL;
			}
			else // if our king was under threat, no need to check if the enemy king is in danger
			{
				ans = this->enemyKingInDanger(board);
			}
		}
	}
	return ans;

}
