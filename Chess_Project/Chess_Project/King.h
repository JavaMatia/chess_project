#pragma once
#include "Base_Player.h"
#define OK '0'
#define INVALID_PIECE_MOVE '6'
#define SIZE 8
#define MATE '8'
#define BLACK_KING 'k'
#define WHITE_KING 'K'

class King :
	public Base_Player
{
public:
	King(const int x, const int y, const char type);
	char move(int x, int y, Board& board, bool kingCheck);
};

